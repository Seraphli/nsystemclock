﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSystemClock
{
    public partial class FormMain : Form
    {
        NtoMSystem numSystem = new NtoMSystem();
        CConfig config= new CConfig("config.xml");
        int numberBase;
        
        public FormMain()
        {
            InitializeComponent();            
        }

        private void ReadConfig()
        {
            if (config.Get("NumberBase") != config.NULL)
            {
                textBoxN.Text = config.Get("NumberBase");
            }
            if (config.Get("Location.X") != config.NULL && config.Get("Location.Y") != config.NULL)
            {
                Location = new Point(Convert.ToInt32(config.Get("Location.X")),
                    Convert.ToInt32(config.Get("Location.Y")));
            }
        }

        private void ClockFresh()
        {
            DateTime timeNow = DateTime.Now;
            DateTime dayBegin = new DateTime(timeNow.Year, timeNow.Month, timeNow.Day, 0, 0, 0);
            TimeSpan ts = timeNow - dayBegin;
            long seconds = (long)ts.TotalSeconds % numSystem.NTo10System("60", numberBase);
            string second = numSystem._10ToNSystem(seconds, numberBase);
            long totalMinutes = (long)ts.TotalSeconds / numSystem.NTo10System("60", numberBase);
            long minutes = totalMinutes % numSystem.NTo10System("60", numberBase);
            string minute = numSystem._10ToNSystem(minutes, numberBase);
            long hours = totalMinutes / numSystem.NTo10System("60", numberBase);
            string hour = numSystem._10ToNSystem(hours, numberBase);
            labelClock.Text = FormatString(hour) + " : " + FormatString(minute) + " : " + FormatString(second);
        }

        private string FormatString(string s)
        {
            if (s.Length < 2)
            {
                if (s.Length == 0)
                {
                    return "00";
                } 
                else
                {
                    return "0" + s;
                }
            }
            return s;
        }

        private void timerClock_Tick(object sender, EventArgs e)
        {
            ClockFresh();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            numberBase = Convert.ToInt32(textBoxN.Text);
            if (numberBase < 2)
            {
                numberBase = 2;
                textBoxN.Text = "2";
            }
            if (numberBase > 36)
            {
                numberBase = 36;
                textBoxN.Text = "36";
            }
            ClockFresh();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            config.Set("NumberBase", textBoxN.Text);
            config.Set("Location.X", Location.X.ToString());
            config.Set("Location.Y", Location.Y.ToString());
            config.Save();
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            ReadConfig();
            numberBase = Convert.ToInt32(textBoxN.Text);
            timerClock.Start();
        }
    }
}
