﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSystemClock
{
    class NtoMSystem
    {
        Dictionary<string, int> NumSystem =
            new Dictionary<string, int>();
        Dictionary<int, string> NameSystem =
            new Dictionary<int, string>();
        string Names = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";

        public NtoMSystem()
        {
            int n = 0;
            foreach (string s in Names.Split(','))
            {
                NumSystem.Add(s, n);
                NameSystem.Add(n, s);
                n++;
            }
        }

        public int GetNumByName(string name)
        {
            return NumSystem[name];
        }

        public string GetNameByNum(int num)
        {
            return NameSystem[num];
        }

        public string NToMSystem(string num, int n, int m)
        {
            return _10ToNSystem(NTo10System(num, n), m);
        }

        public long NTo10System(string num, int n)
        {
            long result = 0;
            foreach (char c in num.ToCharArray())
            {
                result *= n;
                result += GetNumByName(c.ToString());                
            }
            return result;
        }

        public string _10ToNSystem(long num, int n)
        {
            string result = "";
            long temp;
            while (num != 0)
            {
                temp = num % n;
                result = GetNameByNum((int)temp) + result;
                num /= n;
            }
            return result;
        }
    }
}
