# NSystemClock #

This is a clock that running under certain number base system.
Number base can be changed from 2 to 36.

### What is this repository for? ###

* For fun. I was about to sleep one night when the idea came out.
* So you can experience a different kind of clock.
* Your day may become shorter!

### How do I get set up? ###

* Clone repo
* Open project
* Compile
* Run!

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact